package fr.irisa.fluxcampus.MoteurGraph;

import java.util.ArrayList;

import fr.irisa.fluxcampus.hierachie_universitaire.Campus;
import fr.irisa.fluxcampus.interfaceAPI.RecuperationInformation;
import fr.irisa.fluxcampus.structure.FichierBdd;
import fr.irisa.fluxcampus.tools.DuplicateMap;

public class Application2 {
	private static TransactionGraph transactionGraph;
	private static RecuperationInformation recuperation;
	private boolean envoieToAPI = false;
	private boolean prettyCplx = false;
	private String date = null;
	private DuplicateMap<String, String> messageErreur = new DuplicateMap<String, String>();
	private DuplicateMap<String, String> messagePrettyPrint = new DuplicateMap<String, String>();

	public Application2(String cheminSortieGraph, boolean envoieToAPI, boolean prettyCplx, String date) {
		Chemin.graphDossierSortie = cheminSortieGraph;
		this.envoieToAPI = envoieToAPI;
		this.prettyCplx = prettyCplx;
		this.date = date;
	}

	public DuplicateMap<String, String> runFichier(String nomFichier) {
		transactionGraph = new TransactionGraph(nomFichier);
		String fichierAvecExtension = nomFichier + ".ics";

		recuperation = new RecuperationInformation();
		ArrayList<FichierBdd> listeFichier = recuperation.getAllFichier();

		Campus beaulieu = new Campus("Beaulieu", transactionGraph);

		FichierBdd f = FichierBdd.rechercheDansListe(listeFichier, fichierAvecExtension, "2017");
		if (f == null) {
			messageErreur.put("Erreur","fichier null");
		}
		beaulieu.rajouterPromotion(f);
		beaulieu.recupererInformation(date);
		beaulieu.recupererInformationNiveau(fichierAvecExtension);

		beaulieu.creeAgenda();
		beaulieu.creationGraph();
		transactionGraph.creation_Node_End();
		System.out.println("Programme fini");

		transactionGraph.calculSimplexBis(prettyCplx, envoieToAPI);
		messageErreur = transactionGraph.getErreur();
		if (prettyCplx) {
			messagePrettyPrint = transactionGraph.getMessagePrettyPrint();
		}
		messagePrettyPrint= transactionGraph.getMessage();
		messagePrettyPrint.put("PrettyPrint",  "TERMINER");
		return messagePrettyPrint;
	}

	public void runPlusieursFichiers(String[] liste) {
		for (String nomFichier : liste) {
			transactionGraph = new TransactionGraph(nomFichier);
			String fichierAvecExtension = nomFichier + ".ics";

			recuperation = new RecuperationInformation();
			ArrayList<FichierBdd> listeFichier = recuperation.getAllFichier();
			// fr.irisa.fluxcampus.tools.ToolsAffichage.affichageListeFichierBdd(listeFichier);

			Campus beaulieu = new Campus("Beaulieu", transactionGraph);
			// Recuperation FichierBdd (Nom_promo Nom_Formation Nom_Fichier)

			FichierBdd f = FichierBdd.rechercheDansListe(listeFichier, fichierAvecExtension, "2017");
			if (f == null) {
				messageErreur.put("Erreur","fichier null");
			}
			beaulieu.rajouterPromotion(f);
			beaulieu.recupererInformation(date);
			beaulieu.recupererInformationNiveau(fichierAvecExtension);

			beaulieu.creeAgenda();
			beaulieu.creationGraph();
			transactionGraph.creation_Node_End();
			System.out.println("Programme fini");

			transactionGraph.calculSimplexBis(prettyCplx, envoieToAPI);
			messageErreur =transactionGraph.getErreur();
			if (prettyCplx) {
				messagePrettyPrint = transactionGraph.getMessagePrettyPrint();
			}
			messagePrettyPrint.put("PrettyPrint", "TERMINER");
			
		}
	}

	public void runListeFichier(String j) {
	}

	public DuplicateMap<String, String> getMessageErreur() {
		return messageErreur;
	}

	public void setMessageErreur(DuplicateMap<String, String> messageErreur) {
		this.messageErreur = messageErreur;
	}

	public DuplicateMap<String, String> getMessagePrettyPrint() {
		return messagePrettyPrint;
	}

	public void setMessagePrettyPrint(DuplicateMap<String, String> messagePrettyPrint) {
		this.messagePrettyPrint = messagePrettyPrint;
	};

}
