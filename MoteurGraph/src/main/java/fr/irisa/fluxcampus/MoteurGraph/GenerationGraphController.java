package fr.irisa.fluxcampus.MoteurGraph;

import java.util.ArrayList;
import java.util.HashMap;

import fr.irisa.fluxcampus.tools.DuplicateMap;
import fr.irisa.fluxcampus.tools.ReaderCSV;
import fr.irisa.fluxcampus.tools.ToolsAffichage;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class GenerationGraphController {

	public HashMap<String, String> erreur = new HashMap<String, String>();

	@FXML
	private TextArea affichageText;
	@FXML
	private TextArea affichageErreurTextArea;

	@FXML
	private ChoiceBox<String> choixFichier;
	@FXML
	private TextField choixPlusieursFichier;

	@FXML
	private void initialize() {
		if (!InterfaceGraphique.initialisation) {
			affichageText.setText("Il faut d'abord initialiser les différents paramètres.");
		}
		// On initialise la choiceBox avec les fichiers disponibles
		ReaderCSV reader = new ReaderCSV("./fichierICS.csv");
		ArrayList<String> fichierDisponible = reader.getListeCSV();
		choixFichier.setItems(FXCollections.observableArrayList(fichierDisponible));

	}


	@FXML
	private void runFichier() {

		if (!InterfaceGraphique.initialisation) {
			erreur.put("Initlisation", "Il faut d'abord initialiser les différents paramètres.");

		} else {
			String fichierChoisie = choixFichier.getValue();
			InterfaceGraphique.appGraph.runFichier(fichierChoisie);
			DuplicateMap<String, String> messagePrettyPrint;
			String affichagePrettyPrint = null;
			String affichageErreur = null;
			if (InterfaceGraphique.appGraph.getMessagePrettyPrint() != null) {
				messagePrettyPrint = InterfaceGraphique.appGraph.getMessagePrettyPrint();
				affichagePrettyPrint = messagePrettyPrint.toString("PrettyPrint");
				if(affichagePrettyPrint != null)
					affichageText.setText(affichagePrettyPrint);
			}
			DuplicateMap<String, String> messageErreur;
			if (InterfaceGraphique.appGraph.getMessageErreur() != null) {
				messageErreur = InterfaceGraphique.appGraph.getMessageErreur();
				affichageErreur = messageErreur.toString("Erreur");
				if(affichageErreur != null)
					affichageErreurTextArea.setText(affichageErreur);
			}


		}


	}
	@FXML
	private void runPlusieursFichiers() {
		String[] liste = choixPlusieursFichier.getText().split(",");
		int n = Integer.valueOf(liste[0]);
		int m = Integer.valueOf(liste[1]);

	}
}
