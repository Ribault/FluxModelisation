package fr.irisa.fluxcampus.MoteurGraph;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;

import api.requestApi.SendFluxToApi;
import fr.irisa.fluxcampus.hierachie_universitaire.Campus;
import fr.irisa.fluxcampus.hierachie_universitaire.Formation;
import fr.irisa.fluxcampus.hierachie_universitaire.Promotion;
import fr.irisa.fluxcampus.tools.DuplicateMap;
import fr.irisa.fluxcampus.cplexStructure.*;

public class TransactionGraph {

	/** The db. */
	private  GraphDatabaseService db;

	//public File path = new File(Chemin.graphDossierSortie);
	private static final Logger LOG = LogManager.getLogger(TransactionGraph.class);
	private DuplicateMap<String,String> message = new DuplicateMap<String,String>();
	private DuplicateMap<String,String> erreur = new DuplicateMap<String,String>();


	/*
	 * Méthode qui créer un noeud a partire d'une formation passé en argument
	 */
	
	public DuplicateMap<String,String> getMessage()
	{
		return message;
	}
	
	public DuplicateMap<String,String> getErreur()
	{
		return erreur;
	}
	public Node addNodeFormation(TypeDeNoeud type, Formation f,String sFormation) {
		try (Transaction tx = db.beginTx()) {
			Node node = db.createNode(type);
			node.setProperty("Name", f.getNom());
			node.setProperty("formation", sFormation);
			tx.success();
			return node;
		}
	}

	public Node addNode(TypeDeNoeud type, String name) {
		try (Transaction tx = db.beginTx()) {
			Node node = db.createNode(type);
			node.setProperty("Name", name);
			tx.success();
			return node;
		}
	}

	public Node addNodeCampus(TypeDeNoeud campusType, Campus campus) {
		try (Transaction tx = db.beginTx()) {
			Node node = db.createNode(campusType);
			node.setProperty("Name", campus.getName());
			tx.success();
			return node;
		}
	}

	public Node addNodePromotion(TypeDeNoeud type, Promotion p) {
		try (Transaction tx = db.beginTx()) {
			Node node = db.createNode(type);
			node.setProperty("Name", p.getName());
			node.setProperty("niveau", p.getsNiveau());
			node.setProperty("promotion", p.getNomPromotion());
			node.setProperty("fichier", p.getFichier());
			tx.success();
			return node;
		}
	}

	public void creationRelationContains(Node a, Node b) {
		try (Transaction tx = db.beginTx()) {
			a.createRelationshipTo(b, Relation.CONTIENT);
			tx.success();
		}
	}

	public void creationRelationContiensAvecNombreEtudiant(Node a, Node b, int nombreEtudiant) {
		try (Transaction tx = db.beginTx()) {
			Relationship r = a.createRelationshipTo(b, Relation.CONTIENT);
			r.setProperty("etudiant", "" + nombreEtudiant);
			tx.success();
		}
	}

	public void creationRelationContientNombreEtudiant(ArrayList<Node> liste, Node b, int nombreEtudiantAttendue) {

		ArrayList<Node> noeudaSupprimer = new ArrayList<Node>();
		try (Transaction tx = db.beginTx()) {
			for (Node n : liste) {
				String snombreEtudiant = (String) n.getProperty("NombreEtudiants");
				snombreEtudiant = snombreEtudiant.replaceAll("[\\[\\]]", "");
				int nombreEtudiantDansNoeud = Integer.parseInt(snombreEtudiant);
				if (nombreEtudiantDansNoeud > nombreEtudiantAttendue) {
					Relationship r = n.createRelationshipTo(b, Relation.CONTIENT);
					r.setProperty("etudiant", "" + nombreEtudiantAttendue);
					int nouveauNombreEtudiant = nombreEtudiantDansNoeud - nombreEtudiantAttendue;
					n.setProperty("NombreEtudiants", "" + nouveauNombreEtudiant);
					break;

				} else if (nombreEtudiantDansNoeud == nombreEtudiantAttendue) {
					Relationship r = n.createRelationshipTo(b, Relation.CONTIENT);
					r.setProperty("etudiant", "" + nombreEtudiantAttendue);
					n.setProperty("NombreEtudiants", "0");
					liste.remove(n);
					break;
				} else {
					Relationship r = n.createRelationshipTo(b, Relation.CONTIENT);
					r.setProperty("etudiant", "" + nombreEtudiantDansNoeud);
					n.setProperty("NombreEtudiants", "0");
					noeudaSupprimer.add(n);
					nombreEtudiantAttendue -= nombreEtudiantDansNoeud;
				}
			}
			liste.remove(noeudaSupprimer);
			tx.success();
		}
	}

	public void addNodeProperty(Node a, String prop, String defProp) {
		try (Transaction tx = db.beginTx()) {
			a.setProperty(prop, defProp);
			tx.success();
		}
	}

	public TransactionGraph(String fichier) {
		File path = new File(Chemin.graphDossierSortie+"/"+fichier);
		//path.setReadOnly();

		path.setWritable(true);
		message.put("Ecriture possible :", Boolean.toString(path.canWrite()));
		GraphDatabaseFactory dbFactory = new GraphDatabaseFactory();
		db = dbFactory.newEmbeddedDatabase(path);
	}

	


	public Node noeudSortie() {
		try (Transaction tx = db.beginTx()) {
			Node node = db.createNode(TypeDeNoeud.SORTIE);
			node.setProperty("Name", "SORTIE");
			this.setNombreEtudiantDansNoeud(node, 0);
			tx.success();
			return node;
		}
	}

	public int getNombreEtudiantDansNoeud(Node n) {
		try (Transaction tx = db.beginTx()) {
			String b = (String) n.getProperty("NombreEtudiants");
			b = b.replaceAll("[\\[\\]]", "");
			int a = Integer.parseInt(b);
			tx.success();
			return a;
		}
	}

	public int getNombreEtudiantsLibreDansListe(ArrayList<Node> l) {
		int i = 0;
		try (Transaction tx = db.beginTx()) {
			for (Node n : l) {
				String b = (String) n.getProperty("NombreEtudiants");
				b = b.replaceAll("[\\[\\]]", "");
				i += Integer.parseInt(b);
			}
			tx.success();
			return i;
		}
	}

	public int getEtudiantPresentDansNoeud(Node n) {
		try (Transaction tx = db.beginTx()) {
			String b = (String) n.getProperty("EtudiantPresent");
			b = b.replaceAll("[\\[\\]]", "");
			int a = Integer.parseInt(b);
			tx.success();
			return a;
		}
	}

	public void setNombreEtudiantDansNoeud(Node n, int newNumberOfStudent) {
		try (Transaction tx = db.beginTx()) {
			n.setProperty("NombreEtudiants", "" + newNumberOfStudent);
			tx.success();
		}
	}

	// public void creationLiensSur(Node a ,Node n,int nombreEtudiant)
	/**
	 * Creation ndu noeud de sortie
	 */
	public void creation_Node_End() {
		try (Transaction tx = db.beginTx()) {
			// On itére sur les noeuds qui modélisent les dates
			ResourceIterator<Node> noeudDates = db.findNodes(TypeDeNoeud.DATE);
			while (noeudDates.hasNext()) {
				// On crée pour chaque sous graph un noeud de sortie
				Node noeud_de_fin = this.noeudSortie();
				// On récupère un noeud qui reprèsente une date
				Node noeudDate = noeudDates.next();
				// La date
				String date = (String) noeudDate.getProperty("Name");
				// Nombre d'étudiant qui sont rentraient
				int nombre_etudiant_rentree = this.getEtudiantPresentDansNoeud(noeudDate)
						- this.getNombreEtudiantDansNoeud(noeudDate);

				// Définition du parcoureur de graph
				TraversalDescription myMovies = db.traversalDescription().breadthFirst()
						.relationships(Relation.CONTIENT, Direction.OUTGOING)
						.evaluator(Evaluators.excludeStartPosition());
				Traverser traverser = myMovies.traverse(noeudDate);
				// Pour chaque noeud du sous graphe
				for (Node noeud : traverser.nodes()) {
					// Sil il posséde un suivant
					if (noeud.getRelationships(Relation.CONTIENT, Direction.OUTGOING) != null) {
						// On récupère le nombre d'étudiants qui sont dans le
						// noeud
						int nombre_etudiant_restant_dans_le_noeud = getNombreEtudiantDansNoeud(noeud);
						if (nombre_etudiant_restant_dans_le_noeud != 0) {
							// Si il est différents de 0 alors c'est que
							// certeins etudiants sortent
							// On le relie au noeud de sorties
							Relationship r = noeud.createRelationshipTo(noeud_de_fin, Relation.SORTIE);
							// On inscrit sur l'arc sortant le nombre
							// d'étudiants
							r.setProperty("etudiant", nombre_etudiant_restant_dans_le_noeud);
							// On le rajoute au noeud de sortie
							setNombreEtudiantDansNoeud(noeud_de_fin,
									getNombreEtudiantDansNoeud(noeud_de_fin) + nombre_etudiant_restant_dans_le_noeud);
							// this.setNumberOfStudentRemaining(cours, 0);

						}

					}

				}
				// Verification si le nombre d'étudiants qui sortent du graph
				// est égals au nombre rentrant
				int nombre_etudiant_sortie = this.getNombreEtudiantDansNoeud(noeud_de_fin);
				if (nombre_etudiant_sortie != nombre_etudiant_rentree) {
					message.put("Erreur"," NOMBRE ENTREE SORTIE DIFFERENTS A LA DATE  " + date);
				}
			}
			tx.success();
		}
	}

	/**
	 * Search in data.
	 */
	public void searchInData() {
		String retour = "[";
		try (Transaction tx = db.beginTx()) {
			// On récupère le noeud correspondant a la promotion rehercher
			ResourceIterator<Node> noeudPromotions = db.findNodes(TypeDeNoeud.PROMOTION);
			while (noeudPromotions.hasNext()) {
				Node noeudPromotion = noeudPromotions.next();
				String nomPromotion = (String) noeudPromotion.getProperty("Name");
				if (noeudPromotion.getRelationships(Relation.CONTIENT) != null)
					// Pour chaque noeud reliés au noeud de promotion
					// (typiquement ceux des dates)
					for (Relationship relation : noeudPromotion.getRelationships(Relation.CONTIENT,
							Direction.OUTGOING)) {
						// On recupere le noeud
						Node dateNode = relation.getOtherNode(noeudPromotion);
						// La date inscrit dans le noeud
						String date = (String) dateNode.getProperty("Name");
						for (Relationship fluxEntrant : dateNode.getRelationships(Relation.CONTIENT,
								Direction.OUTGOING)) {
							Node noeudEntrant = fluxEntrant.getOtherNode(dateNode);
							String numberStudent = (String) fluxEntrant.getProperty("etudiant");
							String heure_debut = (String) noeudEntrant.getProperty("Heure_debut");

							fr.irisa.fluxcampus.interfaceAPI.Sortie s = new fr.irisa.fluxcampus.interfaceAPI.Sortie(
									"beaulieu", "in", date, heure_debut, numberStudent, "a", nomPromotion);
							retour += s.toJson() + ",";
						}

						TraversalDescription pathDescriptor = db.traversalDescription().breadthFirst()
								.relationships(Relation.CONTIENT, Direction.OUTGOING)
								.evaluator(Evaluators.excludeStartPosition());
						Traverser traverser = pathDescriptor.traverse(dateNode);
						for (Node cours : traverser.nodes()) {
							String heure_fin = (String) cours.getProperty("Heure_fin");
							String b = (String) cours.getProperty("NombreEtudiants");
							b = b.replaceAll("[\\[\\]]", "");

							for (Relationship fluxSortant : cours.getRelationships(Relation.CONTIENT,
									Direction.OUTGOING)) {
								int numberStudent = Integer.valueOf((String) fluxSortant.getProperty("etudiant"));
								fr.irisa.fluxcampus.interfaceAPI.Sortie s = new fr.irisa.fluxcampus.interfaceAPI.Sortie(
										"beaulieu", "out", date, heure_fin, Integer.toString(numberStudent), "a",
										nomPromotion);
								retour += s.toJson() + ",";
							}
						}
					}
			}

			retour = retour.substring(0, retour.length() - 1);
			retour += "]";
			SendFluxToApi.sendFluxToApi(retour, true);
			tx.success();
		}
	}

	public void calculSimplexBis(boolean prettyPrintCplx, boolean envoieToAPI) {
		LOG.debug("calculSimplex en cours");
		ArrayList<fr.irisa.fluxcampus.cplexStructure.SousGraphe> listeDesSousGraphe = new ArrayList<SousGraphe>();
		try (Transaction tx = db.beginTx()) {
			ResourceIterator<Node> listesDesNoeudsDates = db.findNodes(TypeDeNoeud.DATE);

			while (listesDesNoeudsDates.hasNext()) {

				// On récupere un noeud de date qui représente le point d'entré
				// de chaque sous-graphe
				Node noeudDate = listesDesNoeudsDates.next();
				Noeud noeudDepart = new Noeud(noeudDate.getId(), this.getEtudiantPresentDansNoeud(noeudDate),
						noeudDate.getLabels().iterator().next().toString());
				String date = (String) noeudDate.getProperty("Name");
				SousGraphe sGraphe = new SousGraphe(noeudDepart, date, (String) noeudDate.getProperty("promotion"));
				for (Relationship fluxEntrant : noeudDate.getRelationships(Relation.CONTIENT, Direction.OUTGOING)) {

					// On récupère les noeuds directement relié au noeud de
					// depart
					Node nEntrant = fluxEntrant.getOtherNode(noeudDate);
					// On instancie un objet noeud

					Noeud noeudEntrant = new Noeud(nEntrant.getId(), getEtudiantPresentDansNoeud(nEntrant),
							nEntrant.getLabels().iterator().next().toString(),
							(String) nEntrant.getProperty("Heure_debut"), (String) nEntrant.getProperty("Heure_fin"));

					Arc nouvelleArc = new Arc(fluxEntrant.getId(), noeudDepart, noeudEntrant);
					noeudDepart.ajouterArcSortant(nouvelleArc);
					noeudEntrant.ajouterArcEntrant(nouvelleArc);

					sGraphe.addNoeud(noeudEntrant);
					sGraphe.addArc(nouvelleArc);
				}

				// Pour chaque noeud reliés au noeud d'entrée
				TraversalDescription pathDescriptor = db.traversalDescription().breadthFirst()
						.relationships(Relation.CONTIENT, Direction.OUTGOING)
						.relationships(Relation.SORTIE, Direction.OUTGOING)
						.evaluator(Evaluators.excludeStartPosition());
				// .evaluator(Evaluation.EXCLUDE_AND_PRUNE);
				Traverser traverser = pathDescriptor.traverse(noeudDate);

				for (Node cours : traverser.nodes())
					for (Relationship r : cours.getRelationships(Direction.OUTGOING)) {
						Node nodeEntrant = r.getStartNode();
						Node nodeSortant = r.getEndNode();
						if (prettyPrintCplx) {
							message.put("PrettyPrint","entrant : x" + nodeEntrant.getId() + " --->" + "x" + nodeSortant.getId());
							message.put("PrettyPrint","x" + nodeEntrant.getId() + "commence a : "
									+ (String) nodeEntrant.getProperty("Heure_debut") + " fini a : "
									+ (String) nodeEntrant.getProperty("Heure_fin"));
						
						if (!r.isType(Relation.SORTIE))
							message.put("PrettyPrint","x" + nodeSortant.getId() + "commence a : "
									+ (String) nodeSortant.getProperty("Heure_debut") + " fini a : "
									+ (String) nodeSortant.getProperty("Heure_fin"));
						else
							message.put("PrettyPrint","x" + nodeSortant.getId() + "  = sortie");
						}

						Noeud noeudCourantEntrant = sGraphe.searchByID(nodeEntrant.getId());
						Noeud noeudCourantSortant = sGraphe.searchByID(nodeSortant.getId());
						if (noeudCourantSortant == null) {
							if (!r.isType(Relation.SORTIE)) {
								noeudCourantSortant = new Noeud(nodeSortant.getId(),
										getEtudiantPresentDansNoeud(nodeSortant),
										nodeSortant.getLabels().iterator().next().toString(),
										(String) nodeSortant.getProperty("Heure_debut"),
										(String) nodeSortant.getProperty("Heure_fin"));

							} else {

								noeudCourantSortant = new Noeud(nodeSortant.getId(),
										this.getNombreEtudiantDansNoeud(nodeSortant),
										nodeSortant.getLabels().iterator().next().toString());
								sGraphe.setSortant(noeudCourantSortant);
							}
							sGraphe.addNoeud(noeudCourantSortant);
						}
						if (noeudCourantEntrant == null) {
							noeudCourantEntrant = new Noeud(nodeEntrant.getId(),
									getEtudiantPresentDansNoeud(nodeEntrant),
									nodeEntrant.getLabels().iterator().next().toString(),
									(String) nodeEntrant.getProperty("Heure_debut"),
									(String) nodeEntrant.getProperty("Heure_fin"));

							sGraphe.addNoeud(noeudCourantEntrant);

						}
						Arc nouvelleArc = new Arc(r.getId(), noeudCourantEntrant, noeudCourantSortant);
						noeudCourantEntrant.ajouterArcSortant(nouvelleArc);
						noeudCourantSortant.ajouterArcEntrant(nouvelleArc);

						sGraphe.addArc(nouvelleArc);
					}
				listeDesSousGraphe.add(sGraphe);
			}
			tx.success();
		}
		String retour = "[";
		for (SousGraphe ga : listeDesSousGraphe) {

			ga.affectationFluxEntreeSortie();
			if (prettyPrintCplx)
				ga.affichageIdVariables(message);

			for (int j = 0; j < 2; j++)
				for (int i = 0; i < 2; i++) {
					retour += ga.creationCalcul(message,erreur,prettyPrintCplx, i == 0, j == 0);

				}
		}
		retour = retour.substring(0, retour.length() - 1);
		retour += "]";
		message.put("Sortie Json", retour);
		if (envoieToAPI)
			api.requestApi.SendFluxMinMax.sendFluxMinMaxToApi(retour, true);

	}

	public void relierNoeud(Node n, ArrayList<Node> listeNodeEtudiantLibre) {
		try (Transaction tx = db.beginTx()) {
			for (Node d : listeNodeEtudiantLibre) {

				d.createRelationshipTo(n, Relation.CONTIENT);

			}
			tx.success();

		}

	}

	public void viderEtudiantListe(ArrayList<Node> listeNodeEtudiantLibre) {
		try (Transaction tx = db.beginTx()) {
			for (Node n : listeNodeEtudiantLibre) {
				n.setProperty("NombreEtudiants", "" + 0);
			}

			tx.success();
		}

	}
	public DuplicateMap<String,String> getMessagePrettyPrint() {
		return message;
		
	}

}
