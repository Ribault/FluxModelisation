package fr.irisa.fluxcampus.cplexStructure;

import java.io.File;
import java.io.IOException;

import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.commons.io.FileUtils;

public class Json2CSV {
	File fichierDest;
	
	
	Json2CSV()
	{
		
	}
	
	
	public void creeFichierDest(String nomFichier)
	{
		File file = new File(nomFichier);
		fichierDest = file;
	}
	
	
	public void entreeJson(String json){
		JSONArray sortie = new JSONArray(json);

		
		 String csv = CDL.toString(sortie);
         try {
			FileUtils.writeStringToFile(fichierDest, csv);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

}
