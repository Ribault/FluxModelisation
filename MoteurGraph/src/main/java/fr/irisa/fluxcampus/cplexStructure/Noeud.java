package fr.irisa.fluxcampus.cplexStructure;

import java.sql.Time;
import java.util.ArrayList;

import fr.irisa.fluxcampus.MoteurGraph.TypeDeNoeud;
import fr.irisa.fluxcampus.tools.DuplicateMap;
import ilog.concert.*;
import ilog.cplex.*;
public class Noeud {

	private long id;
	private  TypeDeNoeudCplx typeNoeud;
	private ArrayList<Arc> arcEntrant;
	private ArrayList<Arc> arcSortant;
	private int nombreEtudiant;
	private int idVariables;
	private Time heure_debut;
	private Time heure_fin;
	
	public Noeud(long id , int nombreEtudiant,String type)
	{
		this.id = id;
		arcEntrant = new ArrayList<Arc>();
		arcSortant = new ArrayList<Arc>();
		this.setNombreEtudiant(nombreEtudiant);
		TypeDeNoeudCplx t = TypeDeNoeudCplx.affectationStatique(type);
		if(t == null)
		{
			System.out.println("le ttype non connue est :" + type);
		}
	}
	
	public Noeud(long id , int nombreEtudiant,String type,String heure_debut, String heure_fin)
	{
		this.id = id;
		arcEntrant = new ArrayList<Arc>();
		arcSortant = new ArrayList<Arc>();
		this.setNombreEtudiant(nombreEtudiant);
		TypeDeNoeudCplx t = TypeDeNoeudCplx.affectationStatique(type);
		if(t == null)
		{
			System.out.println("le ttype non connue est :" + type);
		}
		this.heure_debut = java.sql.Time.valueOf(heure_debut);
		this.heure_fin = java.sql.Time.valueOf(heure_fin);
	} 
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public ArrayList<Arc> getArcEntrant() {
		return arcEntrant;
	}
	public void setArcEntrant(ArrayList<Arc> arcEntrant) {
		this.arcEntrant = arcEntrant;
	}
	
	public void ajouterArcEntrant(Arc a)
	{
		arcEntrant.add(a);
	}
	public ArrayList<Arc> getArcSortant() {
		return arcSortant;
	}
	public void setArcSortant(ArrayList<Arc> arcSortant) {
		this.arcSortant = arcSortant;
	}
	
	public void ajouterArcSortant(Arc a)
	{
		arcSortant.add(a);
	}

	public int getNombreEtudiant() {
		return nombreEtudiant;
	}

	public void setNombreEtudiant(int nombreEtudiant) {
		this.nombreEtudiant = nombreEtudiant;
	}
	
	public void display(){
		for(Arc a : arcSortant )
			System.out.println("Noeud : " + id + " -->" + a.toString());
	}
	
	public String getType()
	{
		return typeNoeud.toString();
	}
	
	public Time getHeure_debut() {
		return heure_debut;
	}

	public void setHeure_debut(Time heure_debut) {
		this.heure_debut = heure_debut;
	}

	public Time getHeure_fin() {
		return heure_fin;
	}

	public void setHeure_fin(Time heure_fin) {
		this.heure_fin = heure_fin;
	}

	public void initIdVariable(int id)
	{
		idVariables = id;
	}
	
	public int getIdVarible()
	{
		return idVariables;
	}
	
	public void affectationVariable(DuplicateMap<String, String> message, IloNumVar[] variables,char [] typeDeVariables, IloModeler modele,boolean prettyPrint)
	{
		//On affecte la variable du noeud qui se trouve dans le tableau à l'indice  idVariables
		//Puis ceux des arcs entrant
		try {
			variables[idVariables] = modele.numVar(nombreEtudiant, nombreEtudiant,IloNumVarType.Int);
			typeDeVariables[idVariables] = 'x';
			if(prettyPrint)
				message.put("PrettyPrint","x"+idVariables  + "= [" + nombreEtudiant+ "," + nombreEtudiant + "]");
		} catch (IloException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void affectationNoeudSortie(DuplicateMap<String, String> message, IloNumVar[] variables,char [] typeDeVariables, IloModeler modele,boolean prettyPrint)
	{
		//On affecte la variable du noeud qui se trouve dans le tableau à l'indice  idVariables
		//Puis ceux des arcs entrant
		try {
			variables[idVariables] = modele.numVar(0, nombreEtudiant,IloNumVarType.Int);
			typeDeVariables[idVariables] = 'x';
			if(prettyPrint)
				message.put("PrettyPrint","x"+idVariables  + "= [" + 0+ "," + nombreEtudiant + "]");
		} catch (IloException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void affectationNoeudDepart(DuplicateMap<String, String> message, IloNumVar[] variables,char [] typeDeVariables, IloModeler modele,boolean prettyPrint)
	{
		//On affecte la variable du noeud qui se trouve dans le tableau à l'indice  idVariables
		//Puis ceux des arcs entrant
		try {
			variables[idVariables] = modele.numVar(0, nombreEtudiant,IloNumVarType.Int);
			typeDeVariables[idVariables] = 'x';
			if(prettyPrint)
				message.put("PrettyPrint","x"+idVariables  + "= [" + 0+ "," + nombreEtudiant + "]");
		} catch (IloException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void creationContrainte(DuplicateMap<String, String> message, IloNumVar[] variables,char [] tableauTypeVariable, IloModeler modele,boolean prettyPrint)
	{
		//La somme des arc entrent doit être egal au nombre d'étudiants dans le noeud
		try {
			IloLinearNumExpr contr = modele.linearNumExpr();
			//Somme
			String affichageString = ""+ tableauTypeVariable[idVariables] + idVariables + " =" ;
			for(Arc a : arcEntrant)
			{
				
				contr.addTerm(1, variables[a.getIdVarible()]);
				if(prettyPrint)
				{
					affichageString += ""+tableauTypeVariable[a.getIdVarible()]  + a.getIdVarible() +  " +" ; 
				}
			
			}
			//Contrainte d'égalité
			
			if(!arcEntrant.isEmpty())
				modele.addEq(contr, variables[idVariables]);
			
			//Même algo pour les arcs sortant
			IloLinearNumExpr contr2 = modele.linearNumExpr();
			String affichageContrainteSortant =  "x" + idVariables + " =" ;
			for(Arc a : arcSortant)
			{
				contr2.addTerm(1, variables[a.getIdVarible()]);
				if(prettyPrint)
					affichageContrainteSortant += ""+tableauTypeVariable[a.getIdVarible()]  + a.getIdVarible() + " +" ; 
			}
			if(!arcSortant.isEmpty()){
				modele.addEq(contr2, variables[idVariables]);
			}
			
			if(prettyPrint){
				if(!arcEntrant.isEmpty()){
					affichageString = affichageString.substring(0, affichageString.length()-1);
					message.put("PrettyPrint",affichageString);
				}
				if(!arcSortant.isEmpty()){
					affichageContrainteSortant = affichageContrainteSortant.substring(0, affichageContrainteSortant.length() -1);
					message.put("PrettyPrint",affichageContrainteSortant);

				}
			}
				
		} catch (IloException e) {

			e.printStackTrace();
		}
	}
/*	
 * 
 * 
	public ArrayList<IloNumVar> creeCalculArcSortant()
	{
		ArrayList<IloNumVar> retour = new ArrayList<IloNumVar>();
		for(Arc a : arcSortant){
			IloNumVar i = new IloNumVar()
		}
	}
	*/
}
