package fr.irisa.fluxcampus.cplexStructure;
import com.google.gson.Gson;
public class Sortie {
	

	 String localisation;
	 String date;
	 String heure;
	 String effectif;
	 String typeFlux;
	 String tag;
	 String minOrMax;
	 String promo;
	 
	 Sortie(String localisation , String type, String date, String heure,String effectif,boolean minimiser,String promo)
	 {
		 this.localisation = localisation;
		 this.date = date;
		 this.heure = heure;
		 this.effectif = effectif;
		 this.typeFlux = type;

		 this.tag = "0";
		 if(minimiser)
			 minOrMax= "min";
		 else
			 minOrMax = "max";
		 this.promo = promo;
	 }
	 

	 /*
	  * 
	  * 
	  *  {
  "localisation":"campus_beaulieu",
  "typeFlux":"in",
  "date":"2001-01-20",
  "heure":"10:00:00",
  "effectif":"10"
 }
	  */
 	
	 /**
	 * To json.
	 *
	 * @return the string
	 */
 	public String toJson()
	 {
		 Gson gson = new Gson();
		 String json = gson.toJson(this);
		 return json;
	 }
	
}
