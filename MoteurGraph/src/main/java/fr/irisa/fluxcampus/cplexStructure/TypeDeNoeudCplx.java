package fr.irisa.fluxcampus.cplexStructure;

public enum TypeDeNoeudCplx {

	
	FORMATION(null),
	PROMOTION(null),
	DATE(null),
	CAMPUS(null),
	AMPHITHEATRE("CM"),
	MAJ("Mise a Niveau"),
	TD("TD"),
	SORTIE("SORTIE"),
	PROJET("Projet"),
	CC("Controle"),
	TP("TP");
	
	private String name = "";
	
	TypeDeNoeudCplx(String name)
	{
		this.name = name;
	}
	public TypeDeNoeudCplx affectation(String name)
	
	{
		if(name.equals("FORMATION"))
			return TypeDeNoeudCplx.FORMATION;
		else if(name.equals("PROMOTION"))
			return TypeDeNoeudCplx.PROMOTION;
		else if (name.equals("DATE"))
			return TypeDeNoeudCplx.DATE;
		else if (name.equals("CAMPUS"))
			return TypeDeNoeudCplx.CAMPUS;
		else if (name.equals("AMPHITHEATRE"))
			return TypeDeNoeudCplx.AMPHITHEATRE;
		else if (name.equals("MAJ"))
			return TypeDeNoeudCplx.MAJ;
		else if (name.equals("TD"))
			return TypeDeNoeudCplx.TD;
		else if (name.equals("SORTIE"))
			return TypeDeNoeudCplx.SORTIE;
		else if (name.equals("CC"))
			return TypeDeNoeudCplx.CC;
		else if (name.equals("TP"))
			return TypeDeNoeudCplx.TP;
		
		else
		{
			System.err.println("Type inconnue " + name);
			return null;
		}
	}
	
	public static final TypeDeNoeudCplx affectationStatique(String name)
	
	{
		if(name.equals("FORMATION"))
			return TypeDeNoeudCplx.FORMATION;
		else if(name.equals("PROMOTION"))
			return TypeDeNoeudCplx.PROMOTION;
		else if (name.equals("DATE"))
			return TypeDeNoeudCplx.DATE;
		else if (name.equals("CAMPUS"))
			return TypeDeNoeudCplx.CAMPUS;
		else if (name.equals("AMPHITHEATRE"))
			return TypeDeNoeudCplx.AMPHITHEATRE;
		else if (name.equals("MAJ"))
			return TypeDeNoeudCplx.MAJ;
		else if (name.equals("TD"))
			return TypeDeNoeudCplx.TD;
		else if (name.equals("SORTIE"))
			return TypeDeNoeudCplx.SORTIE;
		else if (name.equals("CC"))
			return TypeDeNoeudCplx.CC;
		else if (name.equals("TP"))
			return TypeDeNoeudCplx.TP;
		else if(name.equals("PROJET"))
			return TypeDeNoeudCplx.PROJET;
		else
		{
			System.err.println("Type inconnue " + name);
			return null;
		}
	}
	
	public String toString()
	{
		return name;
	}

}
