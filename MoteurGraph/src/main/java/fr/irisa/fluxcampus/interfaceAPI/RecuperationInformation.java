package fr.irisa.fluxcampus.interfaceAPI;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irisa.fluxcampus.MoteurGraph.TransactionGraph;
import fr.irisa.fluxcampus.structure.FichierBdd;
import model.getEvenNonParse.*;
import model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation;
import model.getInfoPromotion.InfoPromotion;
import model.getInfoUE.InfoUE;
import model.libellecours.LibelleCours;

public class RecuperationInformation {
	private static final Logger LOG = LogManager.getLogger(RecuperationInformation.class);

	public RecuperationInformation() {

	}

	/*
	 * Recupère liste fichiers avec nom promotion/formation et année scolaire et
	 * la renvoie
	 */

	public ArrayList<FichierBdd> getAllFichier() {
		ArrayList<model.getEvenNonParse.EvenementDistinct> listeEvenement = (ArrayList<EvenementDistinct>) api.RequestApi
				.getDisctinctEventParser();
		ArrayList<FichierBdd> listeFichierRetour = new ArrayList<FichierBdd>();
		for (model.getEvenNonParse.EvenementDistinct e : listeEvenement) {
			FichierBdd f = new FichierBdd(e);
			if (!f.existInList(listeFichierRetour))
				listeFichierRetour.add(f);
		}
		return listeFichierRetour;

	}
	
	
	public static final ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation> getGroupe(String  promo, String formation)
	{
		ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation> listeRetour = (ArrayList<GetGroupeByPromoAndFormation>) api.RequestApi.getGroupeByPromoAndFormation(promo, formation);
		if(listeRetour == null)
		{
			listeRetour = new ArrayList<model.getGroupeByPromoAndFormation.GetGroupeByPromoAndFormation>();
		}
		return listeRetour;
	}

	public static final ArrayList<model.getEvenNonParse.EvenementParse> getEvenementParse(String ficher, String annee_scolaire,String date) {
		ArrayList<model.getEvenNonParse.EvenementParse> listeEvenement = (ArrayList<EvenementParse>) api.RequestApi
				.getEventParserWith(ficher, annee_scolaire);
		if(date != null)
		{
			ArrayList<model.getEvenNonParse.EvenementParse> listeEvenementDouble = new ArrayList<EvenementParse>();
			for(model.getEvenNonParse.EvenementParse ev : listeEvenement)
			{
				if(ev.getDate().equals(date))
				{
					listeEvenementDouble.add(ev);
				}
			}
			listeEvenement = listeEvenementDouble;
		}
		return listeEvenement;
	}

	public static final ArrayList<model.getInfoUE.InfoUE> getInformationSurUE(String fichier) {
		ArrayList<model.getInfoUE.InfoUE> liste = (ArrayList<InfoUE>) api.RequestApi.getInfoUEwithParam(fichier);
		return liste;
	}

	
	public static final model.getInfo.getInfo getInformationNiveau(String fichier)
	{
		return api.RequestApi.getInfoNiveau(fichier);
	}
	public static final int getNombreEtudiant(String fichier) {
		LOG.debug("Recuperation nombre d'étudiant dans le fichier : " + fichier);
		ArrayList<model.getInfoPromotion.InfoPromotion> listeInfo = (ArrayList<InfoPromotion>) api.RequestApi
				.getInfoPromotion(fichier);
		if (listeInfo.size() > 1) {
			LOG.error("Erreur element > 1 (" + listeInfo.size() + ")");
		}
		int nombre_etudiant = listeInfo.get(0).getNombre_etudiant();
		LOG.debug("Nombre d'étudiant : " + nombre_etudiant);
		return nombre_etudiant;
	}
	
	public static final ArrayList<model.libellecours.LibelleCours> getLibelleParTag(String fichier)
	{
		LOG.debug("Recuperation des libellés des cours dans le fichier : " + fichier);
		ArrayList<model.libellecours.LibelleCours> listeRetour = (ArrayList<LibelleCours>) api.RequestApi.getLibelleAvecTAG(fichier);
		return listeRetour;
	}

}
