package fr.irisa.fluxcampus.interfaceAPI;


import com.google.gson.Gson;
public class Sortie {
	

	 String localisation;
	 String typeFlux;
	 String date;
	 String heure;
	 String effectif;
	 String tag;
	 String promo;
	 
	 Sortie(String localisation , String type, String date, String heure,String effectif,String promo)
	 {
		 this.localisation = localisation;
		 this.typeFlux = type;
		 this.date = date;
		 this.heure = heure;
		 this.effectif = effectif;
		 this.tag = "";
		 this.promo = promo;
	 }
	 

	 /*
	  * 
	  * 
	  *  {
  "localisation":"campus_beaulieu",
  "typeFlux":"in",
  "date":"2001-01-20",
  "heure":"10:00:00",
  "effectif":"10"
 }
	  */
 	
	 public Sortie(String localisation , String type, String date, String heure,String effectif,String tag,String promo)
	 {
		 
		 this.localisation = localisation;
		 this.typeFlux = type;
		 this.date = date;
		 this.heure = heure;
		 this.effectif = effectif;
		 this.tag = tag;
		 this.promo = promo;
	}


	/**
	 * To json.
	 *
	 * @return the string
	 */
 	public String toJson()
	 {
		 Gson gson = new Gson();
		 String json = gson.toJson(this);
		 return json;
	 }
	
}
