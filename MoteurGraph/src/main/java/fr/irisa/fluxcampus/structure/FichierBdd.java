package fr.irisa.fluxcampus.structure;

import java.util.ArrayList;

public class FichierBdd {

	private String name;
	private String promo;
	private String formation;
	private String annee_scolaire;
	FichierBdd(String name) {
		this.name = name;
	}
	
	public FichierBdd(String name,String promo,String formation)
	{
		this.name = name;
		this.setPromo(promo);
		this.setFormation(formation);
	}
	
	public FichierBdd(model.getEvenNonParse.EvenementDistinct e)
	{
		this.name = e.getFichier();
		this.promo = e.getPromo();
		this.formation = e.getFormation();
		this.annee_scolaire = e.getAnnee_scolaire();
	}

	public String getName()

	{
		return name;
	}
	
	public String toString()
	{
		return name;
	}

	public String getFormation() {
		return formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}

	public String getAnnee_scolaire() {
		return annee_scolaire;
	}

	public void setAnnee_scolaire(String annee_scolaire) {
		this.annee_scolaire = annee_scolaire;
	}
	
	public boolean existInList(ArrayList<FichierBdd> liste)
	{
		for(FichierBdd f : liste)
		{
			if(f.getAnnee_scolaire().equals(this.getAnnee_scolaire()) && 
					f.getFormation().equals(this.getFormation()) &&
					f.getName().equals(this.getName())&&
					f.getPromo().equals(this.getPromo())
					)
				return true;
				
				
				
		}
		return false;
	}
	
	public static final FichierBdd   rechercheDansListe(ArrayList<FichierBdd> liste,String nomFichier,String annee_scolaire)
	{
		for(FichierBdd f : liste)
		{
			if(f.getName().equals(nomFichier) && f.getAnnee_scolaire().equals(annee_scolaire))
			{
				return f;
			}
		}
		return null;
		
	}
}
