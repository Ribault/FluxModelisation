package fr.irisa.fluxcampus.tools;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Code récupèrer içi : https://stackoverflow.com/questions/1669885/what-happens-when-a-duplicate-key-is-put-into-a-hashmap
 *@author : https://stackoverflow.com/users/2123156/java-acm
 */

public class DuplicateMap<K, V> {

    private Map<K, ArrayList<V>> m = new HashMap<>();

    public void put(K k, V v) {
        if (m.containsKey(k)) {
            m.get(k).add(v);
        } else {
            ArrayList<V> arr = new ArrayList<>();
            arr.add(v);
            m.put(k, arr);
        }
    }

     public ArrayList<V> get(K k) {
        return m.get(k);
    }

    public V get(K k, int index) {
        return m.get(k).size()-1 < index ? null : m.get(k).get(index);
    }
    
    public String toString(String key)
    {
    	try {
    	String retour = "";
    	K key2 = extracted(key);
		int taille = this.get(key2).size();
    	for(int i = 0;i< taille;i++ )
    	{
    		retour += m.get(key).get(i) + " \n";
    	}
    	
    	return retour;
    	}
    	catch(Exception e)
    	{
    		return null;
    	}
    }

	private K extracted(String key) {
		return (K) key;
	}
}