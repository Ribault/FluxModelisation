package fr.irisa.fluxcampus.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReaderCSV {
	public String fichierCSV;
	public final String line = "";
	public final String cvsSplitBy = ",";

	public ReaderCSV(String fichier) {
		fichierCSV = fichier;
	}

	public ArrayList<String> getListeCSV() {
		String line = "";
		ArrayList<String> listeRetour = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(fichierCSV))) {

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] fichier = line.split(cvsSplitBy);
				// System.err.println("fichier = " + fichier[0].split("\\.")[0]);

				String fichierSansExtension = fichier[0].split("\\.")[0];
				if (!fichierSansExtension.isEmpty())
					listeRetour.add(fichierSansExtension);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return listeRetour;
	}


}
